import { applyMiddleware, compose, createStore } from 'redux';

import ReduxThunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import rootReducer from './modules';
import { routerMiddleware } from 'react-router-redux';

export const history = createHistory();

const initialState = {};
const enhancers = [];
const middleware = [routerMiddleware(history), ReduxThunk];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

export default createStore(rootReducer, initialState, composedEnhancers);
