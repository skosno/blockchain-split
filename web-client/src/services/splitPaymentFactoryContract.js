import provider, { getAccount } from './web3';

import SplitPaymentFactory from '../contracts/SplitPaymentFactory.json';
import contract from 'truffle-contract';

const splitPaymentFactoryContract = contract(SplitPaymentFactory);
splitPaymentFactoryContract.setProvider(provider);

const factory = splitPaymentFactoryContract.at(
  '0x58ba5a41c9156bfc72ba0afd187ba00920d29cba'
);

const api = {
  create(addresses, shares) {
    /* eslint-disable no-undef */
    return factory.create(addresses, shares, { from: getAccount() });
  },
};

export default api;
