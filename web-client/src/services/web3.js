/* eslint-disable no-undef */
export const web3local = web3;

export function getAccount() {
  return web3local.eth.accounts[0];
}

export default web3local.currentProvider;
