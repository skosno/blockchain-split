import apigClientFactory from 'aws-api-gateway-client';
import { basePathAwsAPI } from '../config';
import { getCognitoCredentials } from './auth';

let credentials = {};
let apiClient = null;

export function getClient() {
  return getCognitoCredentials().then(
    ({ accessKey, secretKey, sessionToken }) => {
      if (
        !credentials.sessionToken ||
        sessionToken !== credentials.sessionToken
      ) {
        credentials = { accessKey, secretKey, sessionToken };
        apiClient = apigClientFactory.newClient({
          accessKey,
          secretKey,
          sessionToken,
          region: 'eu-west-1',
          invokeUrl: basePathAwsAPI,
        });
      }

      return apiClient;
    }
  );
}

export function request(url, options, method, body) {
  return getClient().then(apiClient => {
    return apiClient
      .invokeApi(options, url, method || 'GET', undefined, body)
      .then(result => result.data);
  });
}

export default request;
