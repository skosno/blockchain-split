import AWS from 'aws-sdk';
import hello from 'hellojs';

const awsRegion = 'eu-west-1';
const googleAPI =
  '1026605278944-rmgnvi4gmq6v9l12uk038uc52i5sv38b.apps.googleusercontent.com';
const IdentityPoolId = 'eu-west-1:fedf4749-a641-4ac7-adf9-700a2d7230d6';

AWS.config.region = awsRegion;

export function getCognitoCredentials() {
  return new Promise((resolve, reject) => {
    // Obtain AWS credentials
    AWS.config.credentials.get(function(awsError) {
      if (awsError) {
        console.warn(awsError);
        return reject(awsError);
      }

      const accessKey = AWS.config.credentials.accessKeyId;
      const secretKey = AWS.config.credentials.secretAccessKey;
      const sessionToken = AWS.config.credentials.sessionToken;

      return resolve({
        accessKey,
        secretKey,
        sessionToken,
      });
    });
  });
}

export function cognitoAuthenticate(token) {
  // Add the Google access token to the Cognito credentials login map.
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId,
    Logins: {
      'accounts.google.com': token,
    },
  });

  return getCognitoCredentials();
}

export function googleSuccess(auth) {
  console.warn('google login success', auth);

  // lookup user profile info from the identity provider if not in storage
  const googlePromise = new Promise((resolve, reject) => {
    const user = getUserFromStorage();
    if (user) {
      return resolve(user);
    }

    hello(auth.network)
      .api('/me')
      .then(user => {
        const parsedUser = {
          username: user.email,
        };
        setUserInStorage(parsedUser);
        resolve(parsedUser);
      }, reject);
  });
  const cognitoPromise = cognitoAuthenticate(auth.authResponse.id_token);

  return Promise.all([googlePromise, cognitoPromise]);
}

export function getGoogleAccessToken() {
  const hello = JSON.parse(localStorage.getItem('hello'));

  return (hello && hello.google && hello.google.access_token) || null;
}

export function getUserFromStorage() {
  const user = JSON.parse(localStorage.getItem('user'));

  return user || null;
}

export function setUserInStorage(user) {
  return localStorage.setItem('user', JSON.stringify(user));
}

export function triggerGoogleLogin() {
  return hello('google').login({
    response_type: 'token id_token',
    scope: 'openid email',
    redirect_uri: window.location.href,
  });
}

export function triggerLogout() {
  localStorage.removeItem('user');

  hello('google').logout({
    force: true,
  });

  if (AWS.config.credentials) {
    AWS.config.credentials.clearCachedId();
  }
}

export function checkCredentials(onStartLogin, onLoginSuccess) {
  hello.on('auth.init', onStartLogin);

  // Listen auth.login event, fires when logged
  hello.on('auth.login', auth => {
    googleSuccess(auth).then(([user, cognitoCredentials]) => {
      onLoginSuccess(user);
    });
  });

  hello.on('auth', e => console.warn('auth', e));

  // Initialize hello.js with Google ID
  hello.init({
    google: googleAPI,
  });

  const status = hello('google').getAuthResponse();
  if (status && Number(String(status.expires).replace('.', '')) <= Date.now()) {
    triggerLogout();
  }
}

export default checkCredentials;
