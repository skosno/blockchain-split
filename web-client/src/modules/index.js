import split, { setSplitStateGetter } from './split';

import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import user from './user';

setSplitStateGetter(state => state.split);

export default combineReducers({
  router: routerReducer,
  user,
  split,
});
