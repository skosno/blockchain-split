import {
  checkCredentials,
  getGoogleAccessToken,
  triggerGoogleLogin,
  triggerLogout,
} from '../services/auth';

import { push } from 'react-router-redux';

// Actions
const LOGIN_SUCCESS = 'blockchain-split/user/LOGIN_SUCCESS';
const LOGIN_START = 'blockchain-split/user/LOGIN_START';
const LOGOUT = 'blockchain-split/user/LOGOUT';

const initialState = {
  isAuthenticated: false,
  isAuthenticating: getGoogleAccessToken() ? true : false,
  username: '',
};

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN_START:
      return {
        ...state,
        isAuthenticating: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: true,
        username: action.payload.username,
      };
    case LOGOUT:
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: false,
        username: '',
      };
    default:
      return state;
  }
}

// Action Creators
export function loginStart() {
  return { type: LOGIN_START };
}

export function loginSuccess(user) {
  return { type: LOGIN_SUCCESS, payload: user };
}

export function logoutSuccess() {
  return { type: LOGOUT };
}

// side effects
export function logout() {
  return dispatch => {
    triggerLogout();
    dispatch(logoutSuccess());
    dispatch(push('/login'));
  };
}

export function login() {
  return dispatch => {
    dispatch(loginStart());

    return triggerGoogleLogin();
  };
}

export function checkLoginState(formData) {
  return dispatch => {
    checkCredentials(
      () => dispatch(loginStart()),
      user => {
        dispatch(loginSuccess(user));
        if (window.location.pathname === '/login') {
          dispatch(push('/'));
        }
      }
    );
  };
}
