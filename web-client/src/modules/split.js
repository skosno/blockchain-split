import { generateLocalSelectors } from './../utils/reduxHelper';
import request from '../services/awsRequest';
import splitPaymentFactoryApi from '../services/splitPaymentFactoryContract';

window.spliter = splitPaymentFactoryApi;

// Actions
const ADD_START = 'blockchain-split/split/ADD_START';
const ADD_SUCCESS = 'blockchain-split/split/ADD_SUCCESS';
const LOAD_START = 'blockchain-split/split/LOAD_START';
const LOAD_SUCCESS = 'blockchain-split/split/LOAD_SUCCESS';
const REMOVE_START = 'blockchain-split/split/REMOVE_START';
const REMOVE_SUCCESS = 'blockchain-split/split/REMOVE_SUCCESS';
const PUBLISH_START = 'blockchain-split/split/PUBLISH_START';
const PUBLISH_SUCCESS = 'blockchain-split/split/PUBLISH_SUCCESS';

const initialState = {
  adding: false,
  loading: false,
  contracts: [],
  publishing: {},
};

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_START:
      return {
        ...state,
        adding: true,
      };
    case ADD_SUCCESS:
      return {
        ...state,
        adding: false,
      };
    case REMOVE_START:
    case LOAD_START:
      return {
        ...state,
        loading: true,
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        contracts: action.payload,
      };
    case REMOVE_SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case PUBLISH_START:
      return {
        ...state,
        publishing: {
          ...state.publishing,
          [action.payload]: true,
        },
      };
    case PUBLISH_SUCCESS:
      return {
        ...state,
        publishing: {
          ...state.publishing,
          [action.payload]: false,
        },
      };
    default:
      return state;
  }
}

// Action Creators
export function addStart() {
  return { type: ADD_START };
}

export function addSuccess(contract) {
  return { type: ADD_SUCCESS, payload: contract };
}

export function loadStart() {
  return { type: LOAD_START };
}

export function loadSuccess(contracts) {
  return { type: LOAD_SUCCESS, payload: contracts };
}

export function removeStart() {
  return { type: REMOVE_START };
}

export function removeSuccess(contracts) {
  return { type: REMOVE_SUCCESS };
}

export function publishStart(contractId) {
  return { type: PUBLISH_START, payload: contractId };
}

export function publishSuccess(contract) {
  return { type: PUBLISH_SUCCESS, payload: contract };
}

// side effects
export function loadContracts() {
  return dispatch => {
    dispatch(loadStart());

    return request(`/contracts`, {}, 'GET').then(contracts => {
      dispatch(loadSuccess(contracts));
    });
  };
}

export function addContract(newContract) {
  return dispatch => {
    dispatch(addStart());

    return request(`/contracts`, {}, 'POST', newContract).then(contract => {
      dispatch(addSuccess(contract));
      dispatch(loadContracts());
    });
  };
}

export function removeContract(contractId) {
  return dispatch => {
    dispatch(removeStart());

    return request(`/contracts/${contractId}`, {}, 'DELETE').then(contract => {
      dispatch(removeSuccess());
      dispatch(loadContracts());
    });
  };
}

export function publishContract(contract) {
  return dispatch => {
    dispatch(publishStart(contract.id));

    const addresses = contract.participants.map(
      participant => '' + participant.address
    );
    const shares = contract.participants.map(
      participant => +participant.shares
    );

    return splitPaymentFactoryApi
      .create(addresses, shares)
      .then(transaction => {
        return request(`/contracts/${contract.id}/publish`, {}, 'POST', {
          address: transaction.receipt.logs[0].address,
        });
      })
      .then(() => {
        dispatch(publishSuccess(contract.id));
        dispatch(loadContracts());
      })
      .catch(() => {
        dispatch(publishSuccess(contract.id));
      });
  };
}

// Selectors
export function getContracts(state) {
  return state.contracts;
}

export function getLoading(state) {
  return state.loading;
}

export function getAdding(state) {
  return state.adding;
}

export function getPublishing(state) {
  return state.publishing;
}

let localState = state => state;
export function setSplitStateGetter(getter) {
  localState = getter;
}
export const selectors = generateLocalSelectors(state => localState(state), {
  getContracts,
  getLoading,
  getAdding,
  getPublishing,
});
