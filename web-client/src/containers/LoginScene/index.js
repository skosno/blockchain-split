import './styles.css';

import {
  Button,
  Dimmer,
  Grid,
  Header,
  Icon,
  Loader,
  Segment,
} from 'semantic-ui-react';

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { login } from '../../modules/user';

class LoginScene extends React.Component {
  state = {
    username: '',
    password: '',
  };

  handleChange = e => this.setState({ [e.target.name]: e.target.value });

  submitForm = () =>
    this.props.actions.login({
      username: this.state.username,
      password: this.state.password,
    });

  clearState = () =>
    this.setState({
      username: '',
      password: '',
    });

  render() {
    const { actions, isAuthenticating } = this.props;

    return (
      <Grid verticalAlign="middle" centered className="login-container">
        <Grid.Column className="login-box">
          <Header as="h1">
            <Icon name="lock" />
            <Header.Content>
              Blockchain Split
              <Header.Subheader>Sign in to proceed</Header.Subheader>
            </Header.Content>
          </Header>
          <Segment>
            <Dimmer active={isAuthenticating} inverted>
              <Loader>Loading</Loader>
            </Dimmer>
            {/* <Form onSubmit={this.submitForm}>
            <Form.Field>
              <label>Username</label>
              <input
                type="text"
                name="username"
                placeholder="Username"
                onChange={this.handleChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <input
                type="password"
                name="password"
                placeholder="Password"
                onChange={this.handleChange}
              />
            </Form.Field>
            <Button type="submit" fluid>
              Login
            </Button>
          </Form> */}
            <Button type="button" fluid onClick={actions.login} color="red">
              Login with Google
            </Button>
          </Segment>
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticating: state.user.isAuthenticating,
});

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators({ login }, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScene);
