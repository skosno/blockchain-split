import './styles.css';

import { Link, Route, Switch, withRouter } from 'react-router-dom';
import React, { Component } from 'react';
import { checkLoginState, logout } from '../../modules/user';

import Dashboard from '../DashboardScene';
import Login from '../LoginScene';
import { Menu } from 'semantic-ui-react';
import NoMatch from '../NoMatchScene';
import ProtectedRoute from '../../containers/ProtectedRoute';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class App extends Component {
  constructor(props) {
    super(props);

    props.actions.checkLoginState();
  }

  render() {
    const { isAuthenticated, username, actions } = this.props;

    return (
      <main>
        {isAuthenticated ? (
          <Menu color="teal" inverted>
            <Menu.Item>
              <Link to="/">Blockchain Split</Link>
            </Menu.Item>
            <Menu.Menu position="right">
              <Menu.Item name="user">{username}</Menu.Item>
              <Menu.Item name="logout" onClick={actions.logout}>
                Logout
              </Menu.Item>
            </Menu.Menu>
          </Menu>
        ) : null}

        <Switch>
          <ProtectedRoute exact path="/" component={Dashboard} />
          <Route path="/login" component={Login} />

          <Route component={NoMatch} />
        </Switch>
      </main>
    );
  }
}

function mapStateToProps(state) {
  return {
    isAuthenticated: state.user.isAuthenticated,
    username: state.user.username,
  };
}

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators({ logout, checkLoginState }, dispatch) };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
