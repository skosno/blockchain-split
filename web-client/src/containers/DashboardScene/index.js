import {
  Button,
  Card,
  Container,
  Dimmer,
  Grid,
  Loader,
  Segment,
} from 'semantic-ui-react';
import {
  addContract,
  loadContracts,
  publishContract,
  removeContract,
  selectors,
} from './../../modules/split';

import Contract from './../../components/Contract';
import ContractEditForm from './../../containers/ContractEditForm';
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

export class DashboardScene extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      addContract: false,
    };
  }

  componentDidMount() {
    this.props.actions.loadContracts();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.adding === true && nextProps.adding === false) {
      this.closeAddContract();
    }
  }

  addContract = () => {
    this.setState({ addContract: true });
  };

  closeAddContract = () => {
    this.setState({ addContract: false });
  };

  saveNewContract = newContract => this.props.actions.addContract(newContract);

  removeContract = contractId => this.props.actions.removeContract(contractId);

  publishContract = contract => this.props.actions.publishContract(contract);

  render() {
    const { adding, contracts, loading, publishing } = this.props;
    const { addContract } = this.state;

    return (
      <Container>
        <Grid>
          <Grid.Column>
            <Segment vertical>
              <Dimmer active={adding} inverted>
                <Loader>Saving</Loader>
              </Dimmer>
              {addContract ? (
                <ContractEditForm
                  onCancel={this.closeAddContract}
                  onSubmit={this.saveNewContract}
                />
              ) : (
                <Button type="button" onClick={this.addContract}>
                  Add Contract
                </Button>
              )}
            </Segment>
            <br />
            <div>
              <Dimmer active={loading} inverted>
                <Loader>Loading</Loader>
              </Dimmer>
              <Card.Group>
                {contracts.map(contract => (
                  <Contract
                    contract={contract}
                    key={contract.id}
                    onRemove={e => {
                      e.stopPropagation();
                      this.removeContract(contract.id);
                    }}
                    onPublish={e => {
                      e.stopPropagation();
                      this.publishContract(contract);
                    }}
                    publishing={publishing[contract.id]}
                  />
                ))}
              </Card.Group>
            </div>
          </Grid.Column>
        </Grid>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    contracts: selectors.getContracts(state),
    loading: selectors.getLoading(state),
    adding: selectors.getAdding(state),
    publishing: selectors.getPublishing(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      { addContract, loadContracts, removeContract, publishContract },
      dispatch
    ),
  };
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(DashboardScene)
);
