import { Redirect, Route, withRouter } from 'react-router-dom';

import AuthenticatingLoader from '../../components/AuthenticatingLoader';
import React from 'react';
import { connect } from 'react-redux';

export const ProtectedRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        rest.isAuthenticating ? (
          <AuthenticatingLoader />
        ) : rest.isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

const mapStateToProps = state => ({
  isAuthenticated: state.user.isAuthenticated,
  isAuthenticating: state.user.isAuthenticating,
});

export default withRouter(connect(mapStateToProps)(ProtectedRoute));
