import React from 'react';

const NoMatchScene = () => (
  <div>Page Not Found!</div>
);

export default NoMatchScene;
