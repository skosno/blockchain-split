import { Button, Card, Form, Icon, Label } from 'semantic-ui-react';
import React, { Component } from 'react';

const STATUS_DRAFT = 'draft';

export class ContractEditForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      status: STATUS_DRAFT,
      participants: [
        {
          name: '',
          shares: 0,
          address: '',
        },
      ],
    };
  }

  submitForm = () => {
    this.props.onSubmit(this.state);
  };

  cancelForm = () => this.props.onCancel();

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  addParticipant = () => {
    this.setState({
      participants: [
        ...this.state.participants,
        {
          name: '',
          shares: 0,
          address: '',
        },
      ],
    });
  };

  removeParticipant = index => {
    this.setState({
      participants: [
        ...this.state.participants.slice(0, index),
        ...this.state.participants.slice(index + 1),
      ],
    });
  };

  handleParticipantChange = (index, e) => {
    this.setState({
      participants: [
        ...this.state.participants.slice(0, index),
        {
          ...this.state.participants[index],
          [e.target.name]: e.target.value,
        },
        ...this.state.participants.slice(index + 1),
      ],
    });
  };

  render() {
    const contract = this.state;

    return (
      <Form onSubmit={this.submitForm}>
        <Card fluid raised>
          <Card.Content className="Contract__CardContent">
            <Card.Header>
              <Form.Field>
                <input
                  type="text"
                  name="name"
                  placeholder="Contract Name"
                  value={contract.name}
                  onChange={this.handleChange}
                />
              </Form.Field>
            </Card.Header>
          </Card.Content>
          <Card.Content>
            <Card.Group>
              {contract.participants.map((participant, index) => (
                <Card key={index}>
                  <Card.Content>
                    <Form.Field>
                      <input
                        type="text"
                        name="name"
                        placeholder="Participant Name"
                        value={contract.participants[index].name}
                        onChange={e => this.handleParticipantChange(index, e)}
                      />
                    </Form.Field>
                    <Card.Description>
                      <Form.Field>
                        <input
                          type="number"
                          name="shares"
                          value={contract.participants[index].shares}
                          onChange={e => this.handleParticipantChange(index, e)}
                        />
                      </Form.Field>
                    </Card.Description>
                    <Card.Description>
                      <Form.Field>
                        <input
                          type="text"
                          name="address"
                          placeholder="Participant Address"
                          value={contract.participants[index].address}
                          onChange={e => this.handleParticipantChange(index, e)}
                        />
                      </Form.Field>
                      <Button
                        type="button"
                        onClick={() => this.removeParticipant(index)}
                      >
                        Remove
                      </Button>
                    </Card.Description>
                  </Card.Content>
                </Card>
              ))}
            </Card.Group>
            <br />
            <Button type="button" onClick={this.addParticipant}>
              Add Participant
            </Button>
          </Card.Content>
          <Card.Content extra>
            <Button type="submit" primary>
              Save
            </Button>
            <Button type="button" onClick={this.cancelForm}>
              Cancel
            </Button>
          </Card.Content>
        </Card>
      </Form>
    );
  }
}

export default ContractEditForm;
