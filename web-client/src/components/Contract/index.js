import './styles.css';

import {
  Button,
  Card,
  Dimmer,
  Icon,
  Input,
  Label,
  Loader,
} from 'semantic-ui-react';
import React, { Component } from 'react';

const STATUS_DRAFT = 'draft';

function getTotalShares(participants) {
  return participants.reduce(
    (shares, participant) => shares + participant.shares,
    0
  );
}

export class Contract extends Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: false,
    };
  }

  toggleExpand = () => {
    this.setState({
      expanded: !this.state.expanded,
    });
  };

  render() {
    const { expanded } = this.state;
    const { contract, onRemove, onPublish, publishing } = this.props;

    return (
      <Card fluid raised>
        <Dimmer active={publishing} inverted>
          <Loader>Publishing</Loader>
        </Dimmer>
        <Card.Content
          onClick={this.toggleExpand}
          className="Contract__CardContent"
        >
          {contract.status !== STATUS_DRAFT ? null : (
            <Button
              circular
              icon="trash outline"
              floated="right"
              onClick={onRemove}
            />
          )}
          {/* {contract.status !== STATUS_DRAFT ? null : (
            <Button primary circular icon="edit" floated="right" />
          )} */}
          {contract.status !== STATUS_DRAFT ? null : (
            <Button
              color="red"
              circular
              icon="send"
              floated="right"
              className="Contract__PublishButton"
              onClick={onPublish}
            />
          )}
          <Card.Header>{contract.name}</Card.Header>
          <Card.Meta>{contract.address}</Card.Meta>
        </Card.Content>
        {expanded ? (
          <Card.Content>
            <Card.Group>
              {contract.participants.map((participant, index) => (
                <Card key={index}>
                  <Card.Content>
                    <strong>
                      {participant.name ||
                        `${participant.firstName} ${participant.lastName}`}
                    </strong>
                    <Card.Description>
                      Shares: {participant.shares}
                    </Card.Description>
                    <Card.Description>
                      <Input readOnly fluid value={participant.address} />
                    </Card.Description>
                  </Card.Content>
                </Card>
              ))}
            </Card.Group>
          </Card.Content>
        ) : null}
        <Card.Content extra>
          <Label className="right floated">{contract.status}</Label>
          <span className="Contract__InfoItem">
            <Icon name="users" /> Participants: {contract.participants.length}
          </span>{' '}
          <span className="Contract__InfoItem">
            <Icon name="cubes" />Total Shares:{' '}
            {getTotalShares(contract.participants)}
          </span>
        </Card.Content>
      </Card>
    );
  }
}

export default Contract;
