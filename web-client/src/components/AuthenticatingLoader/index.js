import { Loader } from 'semantic-ui-react';
import React from 'react';

export function AuthenticatingLoader() {
  return (
    <div style={{ textAlign: 'center' }}>
      <Loader active inline>
        Authenticating...
      </Loader>
    </div>
  );
}

export default AuthenticatingLoader;
