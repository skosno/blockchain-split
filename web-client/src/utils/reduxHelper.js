export function generateLocalSelectors(stateGetter, selectors) {
  const selectores = Object.keys(selectors).reduce((result, selectorName) => {
    return {
      ...result,
      [selectorName]: (state, ...args) =>
        selectors[selectorName](stateGetter(state), ...args),
    };
  }, {});

  return selectores;
}
