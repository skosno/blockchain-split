var Artifactor = require('truffle-artifactor');
var artifactor = new Artifactor('./contrats');
var SplitPayment = require('./build/contracts/SplitPayment.json');

console.warn(JSON.stringify(artifactor, null, 2));

artifactor
  .save(SplitPayment, './dist/SplitPayment.js')
  .then(() => console.log('done')); // => a promise
