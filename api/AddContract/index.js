const AWS = require('aws-sdk');
const uuid = require('uuid');
const documentClient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));
  const user = event['cognito-identity'] || null;
  const body = event['body-json'] || null;

  const valid = !!body && body.name;

  if (!user || !valid) {
    return callback(null, {});
  }

  const participants = Array.isArray(body.participants)
    ? body.participants
    : [];

  const contract = {
    id: uuid.v1(),
    owner: user,
    name: '' + body.name,
    participants: participants.map(participant => ({
      name: '' + participant.name,
      shares: +participant.shares,
      address: '' + participant.address,
    })),
    status: 'draft',
    createdAt: new Date().toISOString(),
  };

  const params = {
    TableName: process.env.TABLE_NAME,
    Item: contract,
  };

  console.log('Adding contract:', JSON.stringify(contract, null, 2));

  documentClient.put(params, function(err, data) {
    if (err) {
      console.error(
        'Unable to add item. Error JSON:',
        JSON.stringify(err, null, 2)
      );
      callback(err, null);
    } else {
      console.log('Added contract:', JSON.stringify(contract, null, 2));
      callback(null, contract);
    }
  });
};
