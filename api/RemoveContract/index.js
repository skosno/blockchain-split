const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));
  const user = event['cognito-identity'] || null;
  const id = event['contractId'];

  const valid = !!id;

  if (!user || !valid) {
    return callback(null, {});
  }

  const status = 'deleted';

  const params = {
    TableName: process.env.TABLE_NAME,
    Key: {
      id,
    },
    UpdateExpression: 'set #status = :status',
    ConditionExpression: '#owner = :owner',
    ExpressionAttributeNames: {
      '#status': 'status',
      '#owner': 'owner',
    },
    ExpressionAttributeValues: {
      ':status': status,
      ':owner': user,
    },
    ReturnValues: 'UPDATED_NEW',
  };

  console.log('Removing contract:', JSON.stringify(id, null, 2));

  documentClient.update(params, function(err, data) {
    if (err) {
      console.error(
        'Unable to update item. Error JSON:',
        JSON.stringify(err, null, 2)
      );
      callback(err, null);
    } else {
      console.log('Added contract:', JSON.stringify(data, null, 2));
      callback(null, data);
    }
  });
};
