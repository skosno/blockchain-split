const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));
  const user = event['cognito-identity'] || null;

  if (!user) {
    return callback(null, []);
  }

  const params = {
    TableName: process.env.TABLE_NAME,
    FilterExpression: '#owner = :user and #status <> :status',
    ExpressionAttributeNames: {
      '#owner': 'owner',
      '#status': 'status',
    },
    ExpressionAttributeValues: {
      ':user': user,
      ':status': 'deleted',
    },
  };
  documentClient.scan(params, function(err, data) {
    if (err) {
      callback(err, null);
    } else {
      const items = (data.Items || []).sort(function(a, b) {
        return a.createdAt > b.createdAt
          ? -1
          : a.createdAt < b.createdAt ? 1 : 0;
      });
      callback(null, items);
    }
  });
};
