const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));
  const user = event['cognito-identity'] || null;
  const id = event['contractId'];
  const body = event['body-json'] || null;

  const valid = !!id && !!body && body.address;

  if (!user || !valid) {
    function NotValidRequestError(message) {
      this.name = 'NotValidRequestError';
      this.message = message;
    }
    NotValidRequestError.prototype = new Error();
    return callback(
      new NotValidRequestError('Not a valid request, missing data'),
      null
    );
  }

  const status = 'published';

  const params = {
    TableName: process.env.TABLE_NAME,
    Key: {
      id,
    },
    UpdateExpression: 'set #status = :status, #address = :address',
    ConditionExpression: '#owner = :owner',
    ExpressionAttributeNames: {
      '#status': 'status',
      '#address': 'address',
      '#owner': 'owner',
    },
    ExpressionAttributeValues: {
      ':status': status,
      ':address': body.address,
      ':owner': user,
    },
    ReturnValues: 'UPDATED_NEW',
  };

  console.log('Publishing contract:', JSON.stringify(id, null, 2));

  documentClient.update(params, function(err, data) {
    if (err) {
      console.error(
        'Unable to publish item. Error JSON:',
        JSON.stringify(err, null, 2)
      );
      callback(err, null);
    } else {
      console.log('Published contract:', JSON.stringify(data, null, 2));
      callback(null, data);
    }
  });
};
